package com.example.myschoolapp.data.repository

import com.example.myschoolapp.data.model.NycSchoolSatScoreResponseItem
import com.example.myschoolapp.data.model.NycSchoolsResponseItem
import com.example.myschoolapp.data.model.SchoolService
import com.example.myschoolapp.domain.ISchoolRepository
import javax.inject.Inject

class SchoolRepository @Inject constructor(private val schoolService: SchoolService) :
    ISchoolRepository {

    override suspend fun getSchools(): ArrayList<NycSchoolsResponseItem> {
        return schoolService.getSchoolInfo()
    }

    override suspend fun getSATInfoFromSchool(): ArrayList<NycSchoolSatScoreResponseItem> {
        return schoolService.getSATInfoFromSchool()
    }
}