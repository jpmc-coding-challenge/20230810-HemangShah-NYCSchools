package com.example.myschoolapp.domain.usecase

import android.content.Context
import com.example.myschoolapp.data.model.NycSchoolSatScoreResponseItem
import com.example.myschoolapp.data.repository.SchoolRepository
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class GetSchoolSatInfoUseCase @Inject constructor(
    private val schoolRepository: SchoolRepository,
    @ApplicationContext appContext: Context,
) {
    suspend operator fun invoke(): ArrayList<NycSchoolSatScoreResponseItem> {
        return schoolRepository.getSATInfoFromSchool()
    }
}