package com.example.myschoolapp.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myschoolapp.data.model.NycSchoolSatScoreResponseItem
import com.example.myschoolapp.data.model.NycSchoolsResponseItem
import com.example.myschoolapp.domain.usecase.GetNycSchoolUseCase
import com.example.myschoolapp.domain.usecase.GetSchoolSatInfoUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import javax.inject.Inject

class MyViewModelScope : CoroutineScope {
    private val job = SupervisorJob()
    override val coroutineContext = Dispatchers.Main + job
    fun onDestroy() {
        job.cancel()
    } // Cancel the job when the ViewModelStoreOwner is destroyed    } }
}

@HiltViewModel
class SchoolListViewModel @Inject constructor(
    private val schoolUseCase: GetNycSchoolUseCase

    ) :
    ViewModel() {
    var myViewModelScope: CoroutineScope = MyViewModelScope()
    val schoolInfoLiveData = MutableLiveData<ArrayList<NycSchoolsResponseItem>>()
    val schoolSATInfoLiveData = MutableLiveData<NycSchoolSatScoreResponseItem>()
    val errorLiveData = MutableLiveData<String>("")

    fun getSchools() {
        myViewModelScope.launch {
            try {
                schoolInfoLiveData.postValue(schoolUseCase.invoke())
            } catch (e: Exception) {
                println(e.localizedMessage)
                errorLiveData.postValue("Something went wrong")
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        (myViewModelScope as? MyViewModelScope)?.onDestroy()
    } // Cancel the CoroutineScope when the ViewModel is cleared    }
}