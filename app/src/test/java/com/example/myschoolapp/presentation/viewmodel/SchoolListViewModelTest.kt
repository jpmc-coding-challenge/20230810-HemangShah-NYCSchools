package com.example.myschoolapp.presentation.viewmodel


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.myschoolapp.data.model.NycSchoolsResponseItem
import com.example.myschoolapp.domain.usecase.GetNycSchoolUseCase
import com.example.myschoolapp.domain.usecase.GetSchoolSatInfoUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class SchoolListViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var schoolUseCase: GetNycSchoolUseCase

    @Mock
    lateinit var schoolSATUseCase: GetSchoolSatInfoUseCase

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)
    private val nycSchoolsResponseItem: NycSchoolsResponseItem = Mockito.mock()

    private lateinit var viewModel: SchoolListViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        viewModel = SchoolListViewModel(
            schoolUseCase
        )

        // Replace the ViewModel scope with the TestCoroutineScope
        viewModel.myViewModelScope = testScope
    }

    @After
    fun tearDown() {
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun `test getSchools() success`() = testDispatcher.runBlockingTest {
        // Mock data and behavior for successful school retrieval
        val mockSchools = arrayListOf(nycSchoolsResponseItem, nycSchoolsResponseItem)
        Mockito.`when`(schoolUseCase.invoke()).thenReturn(mockSchools)

        // Call the function to be tested
        viewModel.getSchools()

        // Advance the coroutine dispatcher to execute any coroutines launched inside the function
        testDispatcher.scheduler.advanceUntilIdle()

        // Verify that the schoolInfoLiveData contains the expected data
        assert(viewModel.schoolInfoLiveData.value == mockSchools)
        assert(viewModel.errorLiveData.value == "")
    }

    @Test
    fun `test getSchools() error`() = testDispatcher.runBlockingTest {
        // Mock data and behavior for error during school retrieval
        val errorMessage = "Error fetching schools"
        Mockito.`when`(schoolUseCase.invoke()).thenThrow(RuntimeException(errorMessage))

        // Call the function to be tested
        viewModel.getSchools()

        // Advance the coroutine dispatcher to execute any coroutines launched inside the function
        testDispatcher.scheduler.advanceUntilIdle()

        // Verify that the errorLiveData contains the expected error message
        assert(viewModel.schoolInfoLiveData.value == null)
        assert(viewModel.errorLiveData.value == "Something went wrong")
    }

}
